/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package organizm;

import projektpo.Swiat;
import zwierzeta.Zwierze;

/**
 *
 * @author MychauU
 */


public abstract class Organizm {
    private String ruch;
    private int id;
    private String nazwa;
    private int sila;
    private int inicjatywa;
    private int x;
    private int y;
    private int wiek;
    protected Organizm(Organizm stary){
        x=stary.x;
        y=stary.y;
        id=stary.id;
        sila=stary.sila;
        inicjatywa=stary.inicjatywa;
        nazwa=stary.nazwa;
        wiek=stary.wiek;
        pochodzenie=stary.pochodzenie;
    }
    protected Organizm(int xx, int yy, int idd, int silaa, int inicjatywaa, String wyraz, int wiekk, Swiat swiat){
        x=xx;
        y=yy;
        id=idd;
        sila=silaa;
        inicjatywa=inicjatywaa;
        nazwa=wyraz;
        wiek=wiekk;
        pochodzenie=swiat;
    }
    public Swiat pochodzenie;
    public abstract void akcja();
    public abstract int kolizja(Zwierze kolizujacy);
    public abstract char rysowanie();
    public boolean areEqual(Organizm drugi){
        boolean wynik=false;
        if (x == drugi.x){
            if (y == drugi.y){
		if (id == drugi.id){
                    wynik=true;
                    }
            }
	}
        return wynik;
    }
    public String getNazwa(){
        return nazwa;
    }
    public void setNazwa(String x){
        nazwa=x;
    }
    public int getSila(){
        return sila;
    }
    public void setSila(int x){
        sila=x;
    }
    public int getInicjatywa(){
        return inicjatywa;
    }
    public void setInicjatywa(int x){
        inicjatywa=x;
    }
    public int getWiek(){
        return wiek;
    }
    public void setWiek(int x){
        wiek=x;
    }
    public int getId(){
        return id;
    }
    public void setId(int x){
        id=x;
    }
    public int getX(){
        return x;
    }
    public void setX(int xx){
        x=xx;
    }
    public int getY(){
        return y;
    }
    public void setY(int x){
        y=x;
    }
    public String getRuch(){
        return ruch;
    }
    public void setRuch(String x){
        ruch=x;
    }
    
    
}

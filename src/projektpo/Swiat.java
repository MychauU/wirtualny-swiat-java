package projektpo;

import zwierzeta.Czlowiek;
import zwierzeta.Antylopa;
import zwierzeta.Lis;
import zwierzeta.Owca;
import zwierzeta.Wilk;
import zwierzeta.Zolw;
import rosliny.Mlecz;
import rosliny.Guarana;
import rosliny.WilczaJagoda;
import rosliny.Trawa;
import organizm.Organizm;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.*;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Random;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MychauU
 */
public class Swiat extends javax.swing.JFrame {
    public enum Komunikaty{
        ZABIJ (1),
        UMIERAJ (9999),
        ZUM(666),
        ROZMNAZAJ ( 9003),
        NIC ( 1111),
        UCIEKL(707);
        private final int val;
        private Komunikaty(int v) { val = v; }
        public int getVal() { return val; }
    }
    public enum Tabela_organizmow{
        TRAWA(100),
        MLECZ(101),
        GUARANA(102),
        WILCZAJAGODA(103),
        WILK(201),
	OWCA(202),
	LIS(203),
	ZOLW(204),
	ANTYLOPA(205),
        ;
        private final int val;
        private Tabela_organizmow(int v) { val = v; }
        public int getVal() { return val; }
    }
    protected boolean blokada;
    private Gui gui;
    private int size_active;
    private JFrame window;
    private Organizm []organizmy;
    private Organizm []kolejnosc;
    private int max_x;    
    private int max_y;
    private JButton[][] ButtonsXY;
    private JPanel MapaXY;
    private JPanel Rightpanel;
    private JLabel message;
    private JToolBar tools;
    private JFrame pomFrame;
    int wartoscx=0,wartoscy=0;
    JPanel listPane;
    JTextArea textArea;
    JButton NewGame;
    JButton LoadGame;
    JButton SaveGame;
    Swiat(int x,int y){
        gui = new Gui(this);
        message = new JLabel("Witaj w mojej grze!");
        max_x =x;
        max_y = y;
        blokada=false;
        kolejnosc = new Organizm[max_y*max_x];
        organizmy = new Organizm[max_y*max_x];
        ButtonsXY = new JButton[max_y][max_x];
        size_active = 0;
        dodajOrganizmy();
        initializeGui();
        ustawSwiat();
    }
    private void SaveGameActionPerformed(ActionEvent evt) {
        if (blokada!=true){
            pomFrame=new JFrame("Zapisz Grę, podaj dane");
            pomFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                blokada=false;
            }
            });
            pomFrame.setLocationByPlatform(true);      
            pomFrame.pack();
            pomFrame.add(SaveGamePanel);
            pomFrame.setSize(200, 200);
            pomFrame.setResizable(false);
            pomFrame.setFocusable(false);
            NewGamePanel.setFocusable(false);
            NewGamePanel.setVisible(true);
            pomFrame.setVisible((true));
            blokada=true;  
        }
    }
    private void LoadGameActionPerformed(ActionEvent evt) {
        if (blokada!=true){
            pomFrame=new JFrame("Wczytaj Grę, podaj dane");
            pomFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                blokada=false;
            }
            });
            pomFrame.setLocationByPlatform(true);      
            pomFrame.pack();
            pomFrame.add(LoadGamePanel);
            pomFrame.setSize(200, 200);
            pomFrame.setResizable(false);
            pomFrame.setFocusable(false);
            NewGamePanel.setFocusable(false);
            NewGamePanel.setVisible(true);
            pomFrame.setVisible((true));
            blokada=true;
        }
    }
    private void NewGameActionPerformed(ActionEvent evt) {
        if (blokada!=true){
            pomFrame=new JFrame("Nowa Gra, podaj dane");
            pomFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                blokada=false;
            }
            });
            pomFrame.setLocationByPlatform(true);      
            pomFrame.pack();
            pomFrame.add(NewGamePanel);
            pomFrame.setSize(200, 200);
            pomFrame.setResizable(false);
            pomFrame.setFocusable(false);
            NewGamePanel.setFocusable(false);
            NewGamePanel.setVisible(true);
            pomFrame.setVisible((true));
            blokada=true;
        }
    }
    private void CreateOrganizmActionPerformed(ActionEvent evt){
        if (blokada!=true){
            pomFrame=new JFrame("Stworz Organizm");
            pomFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                blokada=false;
                wartoscx=0;
                wartoscy=0;
            }
            });
            JButton button= (JButton) evt.getSource();
            wartoscx = (Integer) button.getClientProperty( "firstIndex" );
            wartoscy = (Integer) button.getClientProperty( "secondIndex" );
            pomFrame.setLocationByPlatform(true);      
            pomFrame.pack();
            pomFrame.add(CreateOrganizmPanel);
            pomFrame.setSize(200, 300);
            pomFrame.setResizable(false);
            pomFrame.setFocusable(false);
            NewGamePanel.setFocusable(false);
            NewGamePanel.setVisible(true);
            pomFrame.setVisible((true));
            blokada=true;
        }
    }
    private void initializeGui() {
        NewGame=new JButton("New");
        NewGame.addActionListener(this::NewGameActionPerformed);
        LoadGame=new JButton("Load");
        LoadGame.addActionListener(this::LoadGameActionPerformed);
        SaveGame=new JButton("Save");
        SaveGame.addActionListener(this::SaveGameActionPerformed);
        gui.setFocusable(true);
        gui.setLayout(new BoxLayout(gui, BoxLayout.PAGE_AXIS));
        listPane = new JPanel();
        listPane.setLayout(new BoxLayout(listPane, BoxLayout.LINE_AXIS));
        gui.setBorder(new EmptyBorder(5, 5, 5, 5));
        tools = new JToolBar();
        tools.setFloatable(false);
        gui.add(tools, BorderLayout.NORTH);
        tools.add(NewGame);
        tools.add(SaveGame);
        tools.add(LoadGame);
        tools.addSeparator();
        tools.add(message);
        textArea=new JTextArea(10,0);
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setBorder(new LineBorder(Color.BLACK));
        MapaXY = new JPanel(new GridLayout(0, max_x));
        MapaXY.setBorder(new LineBorder(Color.BLACK));
    //    Rightpanel= new JPanel(); 
    //    Rightpanel.setBorder(new LineBorder(Color.BLACK));
        listPane.add(MapaXY);
   //     listPane.add(Rightpanel);
        gui.add(listPane);
        gui.add(textArea);
        Insets buttonMargin = new Insets(0,0,0,0);
        for (int ii = 0; ii < max_y; ii++) {
            for (int jj = 0; jj < max_x; jj++) {
                JButton b;
                if (organizmy[jj + ii*max_x]!=null){
                    b = new JButton(String.valueOf(organizmy[jj + ii*max_x].rysowanie()));
                    b.setHorizontalTextPosition( SwingConstants.CENTER );
                    if (organizmy[jj + ii*max_x].getId()<10)
                        b.setForeground(Color.RED);
                    else if (organizmy[jj + ii*max_x].getId()>=100 && organizmy[jj + ii*max_x].getId()<200)
                       b.setForeground(new Color(0,154,0));
                    else {
                        b.setForeground(Color.BLACK);
                    }
                }
                else b=new JButton();
                b.setMargin(buttonMargin);
                b.setBorder(null);
                ImageIcon icon = new ImageIcon(
                        new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB));
                b.setIcon(icon);
                    b.setBackground(new Color(234,234,234));
                b.setPreferredSize(new Dimension(40, 40));
                b.addActionListener(this::CreateOrganizmActionPerformed);
                b.putClientProperty("firstIndex", jj);
                b.putClientProperty("secondIndex", ii);    
                ButtonsXY[ii][jj] = b;
            }
        }
        
    
        for (int ii = 0; ii < max_y; ii++) {
            for (int jj = 0; jj < max_x; jj++) {
                         MapaXY.add(ButtonsXY[ii][jj]);
                         
            }
        }
        JScrollPane areaScrollPane = new JScrollPane ( textArea );
        areaScrollPane.setVerticalScrollBarPolicy ( ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED );
        gui.add(areaScrollPane);
    }
    private void rysujMape(){
        
        window = new JFrame("Michał Dobreńko 155056");
        window.add(gui);
        initComponents();
     //   Rightpanel.add(jPanel1);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLocationByPlatform(true);
        window.pack();
      //  window.setExtendedState(Frame.MAXIMIZED_BOTH);
      //  window.setMinimumSize(window.getSize());
        window.setSize(600, 600);
     //  window.setResizable(false);
        window.setVisible(true);
    }
    public  JTextArea getTextArea(){
        return  textArea;
    }
    public int getXsize(){
        return max_x;
    }
    public int getYsize(){
        return max_y;
    }
    public Organizm[] getOrg(){
        return organizmy;
    }
    public Organizm[] getKol(){
        return kolejnosc;
    }
    public int getSizeOfHeap(){
        return size_active;
    }
    private int losujIndex(){
        Random generator=new Random();
        int losuj;
        do{
            losuj = generator.nextInt(max_x*max_y);
        } while (organizmy[losuj] != null);
        return losuj;
    }
    private void ustawStos(){
        for (int i = 0; i < max_x*max_y; i++){
            if (organizmy[i]!=null)
                heap_insert(organizmy[i]);
        }
    }
    private void ustawSwiat(){
        rysujMape();
    }
    private void updateMapXY(){
        ImageIcon icon = new ImageIcon(
                        new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB));
         for (int ii = 0; ii < max_y; ii++) {
            for (int jj = 0; jj < max_x; jj++) {
                if (organizmy[jj + ii*max_x]!=null){
                     ButtonsXY[ii][jj].setHorizontalTextPosition( SwingConstants.CENTER );
                    ButtonsXY[ii][jj].setText(String.valueOf(organizmy[jj + ii*max_x].rysowanie()));
                    if (organizmy[jj + ii*max_x].getId()<10)
                       ButtonsXY[ii][jj].setForeground(Color.RED);
                    else if (organizmy[jj + ii*max_x].getId()>=100 && organizmy[jj + ii*max_x].getId()<200)
                       ButtonsXY[ii][jj].setForeground(new Color(0,154,0));
                    else {
                        ButtonsXY[ii][jj].setForeground(Color.BLACK);
                    }
                   // ButtonsXY[ii][jj].setIcon(icon);
                }
                else //ButtonsXY[ii][jj].setIcon(icon);
                    ButtonsXY[ii][jj].setText(null);
            }
        }
    }
    public void wykonajTure(){
        Organizm rm;
        if (kolejnosc[0]!=null){
            kolejnosc=null;
            kolejnosc=new Organizm[max_y*max_x];
        }
        ustawStos();
        while (size_active != 0){
                rm = heap_remove();
                kolejnosc[size_active] = null;
                if (rm!=null)
                    rm.akcja();
        }
        updateMapXY();
        window.getContentPane().validate();
        window.getContentPane().repaint();
        MapaXY.validate();
        MapaXY.repaint();
    }
    private void dodajOrganizmy(){
		int losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Trawa(losuj%max_x, losuj / max_x, this);
                losuj = losujIndex();
		organizmy[losuj] = new Mlecz(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Mlecz(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Mlecz(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Mlecz(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new WilczaJagoda(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new WilczaJagoda(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new WilczaJagoda(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Guarana(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Guarana(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Guarana(losuj%max_x, losuj / max_x, this);
                losuj = losujIndex();
		organizmy[losuj] = new Wilk(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Wilk(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Wilk(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Owca(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Owca(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Owca(losuj%max_x, losuj / max_x, this);
               losuj = losujIndex();
		organizmy[losuj] = new Zolw(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Zolw(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Zolw(losuj%max_x, losuj / max_x, this);
                losuj = losujIndex();
		organizmy[losuj] = new Antylopa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Antylopa(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Antylopa(losuj%max_x, losuj / max_x, this);
                losuj = losujIndex();
		organizmy[losuj] = new Lis(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Lis(losuj%max_x, losuj / max_x, this);
		losuj = losujIndex();
		organizmy[losuj] = new Lis(losuj%max_x, losuj / max_x, this);
                losuj = losujIndex();
		organizmy[losuj] = new Czlowiek(losuj%max_x, losuj / max_x, this);
    }
    private void stworzOrganizm(){
        int index=JListOrganizmy.getSelectedIndex();
        if (index!=-1){
            if (organizmy[wartoscx+wartoscy*max_x]==null){
                if (index==0){
                    organizmy[wartoscx+wartoscy*max_x]=new Wilk(wartoscx,wartoscy,this);
                    textArea.append("Stworzyles Wilka na pozycji X "+wartoscx+", Y "+wartoscy+".\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    
                }
                else if (index==1){
                    organizmy[wartoscx+wartoscy*max_x]=new Lis(wartoscx,wartoscy,this);
                    textArea.append("Stworzyles Lisa na pozycji X "+wartoscx+", Y "+wartoscy+".\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    
                }
                else if (index==2){
                    organizmy[wartoscx+wartoscy*max_x]=new Owca(wartoscx,wartoscy,this);
                    textArea.append("Stworzyles Owce na pozycji X "+wartoscx+", Y "+wartoscy+".\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    
                }
                else if (index==3){
                    organizmy[wartoscx+wartoscy*max_x]=new Antylopa(wartoscx,wartoscy,this);
                    textArea.append("Stworzyles Antylope na pozycji X "+wartoscx+", Y "+wartoscy+".\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    
                }
                else if (index==4){
                    organizmy[wartoscx+wartoscy*max_x]=new Zolw(wartoscx,wartoscy,this);
                    textArea.append("Stworzyles Zolwia na pozycji X "+wartoscx+", Y "+wartoscy+".\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    
                }
                else if (index==5){
                    organizmy[wartoscx+wartoscy*max_x]=new Trawa(wartoscx,wartoscy,this);
                    textArea.append("Stworzyles Trawe na pozycji X "+wartoscx+", Y "+wartoscy+".\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    
                }
                else if (index==6){
                    organizmy[wartoscx+wartoscy*max_x]=new Mlecz(wartoscx,wartoscy,this);
                    textArea.append("Stworzyles Mlecz na pozycji X "+wartoscx+", Y "+wartoscy+".\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    
                }
                else if (index==7){
                    organizmy[wartoscx+wartoscy*max_x]=new Guarana(wartoscx,wartoscy,this);
                    textArea.append("Stworzyles Guarane na pozycji X "+wartoscx+", Y "+wartoscy+".\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    
                }
                else if (index==8){
                    organizmy[wartoscx+wartoscy*max_x]=new WilczaJagoda(wartoscx,wartoscy,this);
                    textArea.append("Stworzyles Wilcza Jagode na pozycji X "+wartoscx+", Y "+wartoscy+".\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    
                }
                heap_insert(organizmy[wartoscx+wartoscy*max_x]);
                blokada=false;
                pomFrame.dispose(); 
                pomFrame=null;
                wartoscx=0;
                wartoscy=0;
                updateMapXY();
            }
            else{
                textArea.append("Wybrane pole X "+wartoscx+", Y "+wartoscy+" bylo juz zajete przez Organizm, dlatego utworzenie nie powiodlo sie\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
                pomFrame.dispose(); 
                pomFrame=null;
                wartoscx=0;
                wartoscy=0;
                blokada=false;
            }
        }else{
            textArea.append("Prosze cos wybrac z listy\n");
            textArea.setCaretPosition(textArea.getDocument().getLength());
            JListOrganizmy.requestFocusInWindow();
            textArea.setFocusable(false);
        }
    }
    private void zapiszGre(){
        String wx=jTextField4.getText();
        if (wx.compareTo("") == 0){
            textArea.append("Pusta nazwa pliku\n");
            textArea.setCaretPosition(textArea.getDocument().getLength());
            jTextField4.requestFocusInWindow();
            textArea.setFocusable(false);
        }
        else{
            BufferedWriter writer = null;
            try
            {
                writer = new BufferedWriter( new FileWriter(wx));
                String wypisz=max_x+" "+max_y;
                writer.write(wypisz);
                writer.newLine();
                for(int i=0;i<max_x*max_y;i++){
                    if (organizmy[i] != null){
                    wypisz=organizmy[i].getId()+" "+organizmy[i].getX()+" "+organizmy[i].getY()+" "+organizmy[i].getSila()+" "+organizmy[i].getWiek();
                    writer.write(wypisz);
                    writer.newLine();
                    }
                }
                textArea.append("Zapis pod nazwą pliku: "+wx+" powiódł się!\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
                pomFrame.dispose();
                pomFrame=null;
                blokada=false;
                textArea.setFocusable(false);
                window.requestFocus();
                
            }
            catch ( IOException e){
                textArea.append("Zapis pod nazwą pliku: "+wx+" nie powiódł się!\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
            }
            finally{
                try{
                    if ( writer != null)
                    writer.close( );
                }
                catch ( IOException e){
                    textArea.append("Zapis pod nazwą pliku: "+wx+" nie powiódł się!\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                }
            }
        }
    }
    private void wczytajGre(){
        BufferedReader br = null;
        String wx=jTextField3.getText();
        if (wx.compareTo("") == 0){
            textArea.append("Pusta nazwa pliku\n");
            textArea.setCaretPosition(textArea.getDocument().getLength());
            jTextField4.requestFocusInWindow();
            textArea.setFocusable(false);
        }
        else{
            try {
            br = new BufferedReader(new FileReader(wx));
            StringBuilder hs = new StringBuilder();
            String line = br.readLine();
            hs.append(line);
            hs.append(System.lineSeparator());
            String[] podziel = line.split(" ");
            if (Integer.parseInt(podziel[0])<30 && Integer.parseInt(podziel[1])<30 && Integer.parseInt(podziel[0])>9 && Integer.parseInt(podziel[1])>9){
                max_x=Integer.parseInt(podziel[0]);
                max_y=Integer.parseInt(podziel[1]);
                organizmy=null;
                kolejnosc=null;
                organizmy=new Organizm[max_x*max_y];
                kolejnosc=new Organizm[max_x*max_y];
            while (line != null) {
                line = br.readLine();
                if (line == null) break;
                hs.append(line);
                hs.append(System.lineSeparator());
                podziel = line.split(" ");
                int pomId=Integer.parseInt(podziel[0]);
                int pomX=Integer.parseInt(podziel[1]);
                int pomY=Integer.parseInt(podziel[2]);
                int pomSila=Integer.parseInt(podziel[3]);
                int pomWiek=Integer.parseInt(podziel[4]);
                if (pomId== Tabela_organizmow.WILK.getVal()) {
                    this.organizmy[pomX+pomY*max_x] = new Wilk(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
                else if (pomId== Tabela_organizmow.ANTYLOPA.getVal()) {
                    this.organizmy[pomX+pomY*max_x] = new Antylopa(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
                else if (pomId== Tabela_organizmow.ZOLW.getVal()) {
                    this.organizmy[pomX+pomY*max_x] = new Zolw(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
                else if (pomId== Tabela_organizmow.LIS.getVal()) {
                    this.organizmy[pomX+pomY*max_x] = new Lis(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
                else if (pomId<10) {
                    this.organizmy[pomX+pomY*max_x] = new Czlowiek(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
                else if (pomId== Tabela_organizmow.MLECZ.getVal()) {
                    this.organizmy[pomX+pomY*max_x] = new Mlecz(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
                else if (pomId== Tabela_organizmow.TRAWA.getVal()) {
                    this.organizmy[pomX+pomY*max_x] = new Trawa(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
                else if (pomId== Tabela_organizmow.GUARANA.getVal()) {
                    this.organizmy[pomX+pomY*max_x] = new Guarana(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
                else if (pomId== Tabela_organizmow.WILCZAJAGODA.getVal()) {
                    this.organizmy[pomX+pomY*max_x] = new WilczaJagoda(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
                else if (pomId== Tabela_organizmow.OWCA.getVal()) {
                    this.organizmy[pomX+pomY*max_x] = new Owca(pomX, pomY, this);
                    this.organizmy[pomX+pomY*max_x].setSila(pomSila);
                    this.organizmy[pomX+pomY*max_x].setWiek(pomWiek);
                }
            }        
            String everything = hs.toString();
            window.dispose();
            pomFrame.dispose(); 
            pomFrame=null;
            gui=null;
            message=null;
            ButtonsXY=null;
            MapaXY=null;
            Rightpanel=null;
            window=null;
            message=null;
            tools=null;
            listPane=null;
            textArea=null;
            NewGame=null;
            size_active=0;
            blokada=false;
            gui = new Gui(this);
            message = new JLabel("Witaj w mojej grze!");
            wartoscx=0;
            wartoscy=0;
            ButtonsXY = new JButton[max_y][max_x];
            size_active = 0;
            initializeGui();
            ustawSwiat();
            textArea.append("Wczytano poprawnie plik o nazwie "+wx+"\n");
            textArea.setCaretPosition(textArea.getDocument().getLength());
            textArea.setFocusable(false);
            }
            else{
                textArea.append("Nie udało się wczytać pliku -> rozmiar planszy za duży >30 lub za maly <9\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
                pomFrame.dispose();
                pomFrame=null;
                blokada=false;
                textArea.setFocusable(false);
            }
        } catch (Exception e) {
            textArea.append("Nie udało się wczytać pliku -> błąd wewnętrzny\n");
            textArea.setCaretPosition(textArea.getDocument().getLength());
            jTextField4.requestFocusInWindow();
            textArea.setFocusable(false);
        }
        }
    }
    private void heapify(int i, int size){
        int maxps=i;
        int L = 2 * i;
        int P = 2 * i + 1;
        int wiek;
        int inicjatywa;
        
        if (kolejnosc[i - 1] == null) {
            if (L <= size && kolejnosc[L - 1] != null){
                maxps = L;
            }
            if (P <= size && kolejnosc[P - 1] != null){
                maxps = P;
            }
            else {
                maxps = i;
            }
        }
        else {
            wiek=kolejnosc[i - 1].getWiek();
            inicjatywa=kolejnosc[i - 1].getInicjatywa();
            if (L <= size && kolejnosc[L - 1] == null){
                heapify(L, size);
            }
            else if (L <= size && kolejnosc[L - 1].getInicjatywa() > kolejnosc[i - 1].getInicjatywa()){
                if (inicjatywa<=kolejnosc[L - 1].getInicjatywa()){
                    maxps = L;
                    inicjatywa=kolejnosc[L - 1].getInicjatywa();
                }
            }
            else {
                maxps = i;
            }
            if (L <= size && kolejnosc[L - 1] == null){
                heapify(L, size);
            }
            else if (L <= size && kolejnosc[L - 1].getInicjatywa() == kolejnosc[i - 1].getInicjatywa()){
                if (inicjatywa<=kolejnosc[L - 1].getInicjatywa()){
                    inicjatywa=kolejnosc[L - 1].getInicjatywa();
                    if (L <= size && kolejnosc[L - 1].getWiek() > kolejnosc[i - 1].getWiek()) {
                        if (wiek<=kolejnosc[L - 1].getWiek()){  
                        wiek=kolejnosc[L - 1].getWiek();
                        maxps = L;
                        }       
                    }
                }
            }
            if (L <= size && kolejnosc[P - 1] == null){
                heapify(P, size);
            }
            else if (P <= size && kolejnosc[P - 1].getInicjatywa() > kolejnosc[i - 1].getInicjatywa()) {
                if (inicjatywa<=kolejnosc[P - 1].getInicjatywa()){
                    maxps = P;
                    inicjatywa=kolejnosc[P - 1].getInicjatywa();
                }       
            }
            else if (P <= size && kolejnosc[P - 1].getInicjatywa() == kolejnosc[i - 1].getInicjatywa()){
                if (inicjatywa<=kolejnosc[P - 1].getInicjatywa()){
                    inicjatywa=kolejnosc[P - 1].getInicjatywa();
                    if (P <= size && kolejnosc[P - 1].getWiek() > kolejnosc[P - 1].getWiek()) {
                        if (wiek<=kolejnosc[P - 1].getWiek()){  
                        wiek=kolejnosc[P - 1].getWiek();
                        maxps = P;
                        }       
                    }
                }
            }
        }
        if (maxps != i){
        //    swappointers(kolejnosc[i - 1], kolejnosc[maxps - 1]);
            swappointers(kolejnosc,i-1,maxps-1);
            heapify(maxps, size);
        }
    }
    protected void heap_build(){
        for (int i = size_active / 2; i > 0; i /= 2){
            heapify(i,size_active);
        }
    }
    private void heap_insert(Organizm x){
        kolejnosc[size_active] = x;
        size_active++;
        heap_build();
    }
    private Organizm heap_remove(){
        if (size_active == 0)
            return null;
        else if (kolejnosc[0]==null) {
            --size_active;
            swappointers(kolejnosc,0,size_active);
            heap_build();
            return null;
            }
        --size_active;
        swappointers(kolejnosc,0,size_active);
        heap_build();
        return kolejnosc[size_active];
    }
    public Organizm seekHuman(){
		for (int i = 0; i < max_x*max_y; i++){
			if (organizmy[i]!= null){
				if (organizmy[i].getId() < 6){
					return organizmy[i];
				}
			}
		}
		return null;
	}
    public static final <T> void swappointers(T[] organizmy, int i,int j){
          T t = organizmy[i];
            organizmy[i] = organizmy[j];
            organizmy[j] = t;
    }
    public static boolean isNumeric(String str)
    {
      NumberFormat formatter = NumberFormat.getInstance();
      ParsePosition pos = new ParsePosition(0);
      formatter.parse(str, pos);
      return str.length() == pos.getIndex();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        NewGamePanel = new javax.swing.JPanel();
        jTextField2 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        LoadGamePanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        SaveGamePanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        CreateOrganizmPanel = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        JListOrganizmy = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jButton1.setText("Stworz");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jLabel2.setText("Podaj Y");

        jLabel1.setText("podaj X");

        jLabel5.setText("Po wpisaniu naciśnij enter");

        javax.swing.GroupLayout NewGamePanelLayout = new javax.swing.GroupLayout(NewGamePanel);
        NewGamePanel.setLayout(NewGamePanelLayout);
        NewGamePanelLayout.setHorizontalGroup(
            NewGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NewGamePanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton1))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, NewGamePanelLayout.createSequentialGroup()
                .addGroup(NewGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(NewGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField2)
                    .addComponent(jTextField1)))
            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        NewGamePanelLayout.setVerticalGroup(
            NewGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(NewGamePanelLayout.createSequentialGroup()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(NewGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(NewGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(12, 12, 12))
        );

        jLabel3.setText("Podaj nazwę pliku typu *.txt");

        jLabel6.setText("I wciśnij enter::");

        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout LoadGamePanelLayout = new javax.swing.GroupLayout(LoadGamePanel);
        LoadGamePanel.setLayout(LoadGamePanelLayout);
        LoadGamePanelLayout.setHorizontalGroup(
            LoadGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(LoadGamePanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        LoadGamePanelLayout.setVerticalGroup(
            LoadGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LoadGamePanelLayout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jLabel4.setText("Podaj nazwę pliku typu *.txt");

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jLabel7.setText("I wciśnij enter:");

        javax.swing.GroupLayout SaveGamePanelLayout = new javax.swing.GroupLayout(SaveGamePanel);
        SaveGamePanel.setLayout(SaveGamePanelLayout);
        SaveGamePanelLayout.setHorizontalGroup(
            SaveGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
            .addGroup(SaveGamePanelLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        SaveGamePanelLayout.setVerticalGroup(
            SaveGamePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(SaveGamePanelLayout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jButton2.setText("Utwórz");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        JListOrganizmy.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Wilk", "Lis", "Owca", "Antylopa", "Zolw", "Trawa", "Mlecz", "Guarana", "WilczaJagoda" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        JListOrganizmy.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        JListOrganizmy.setToolTipText("");
        jScrollPane1.setViewportView(JListOrganizmy);

        javax.swing.GroupLayout CreateOrganizmPanelLayout = new javax.swing.GroupLayout(CreateOrganizmPanel);
        CreateOrganizmPanel.setLayout(CreateOrganizmPanelLayout);
        CreateOrganizmPanelLayout.setHorizontalGroup(
            CreateOrganizmPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CreateOrganizmPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
            .addComponent(jScrollPane1)
        );
        CreateOrganizmPanelLayout.setVerticalGroup(
            CreateOrganizmPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CreateOrganizmPanelLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton2))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(NewGamePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(234, 234, 234)
                        .addComponent(CreateOrganizmPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(LoadGamePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(115, 115, 115))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(SaveGamePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(111, 111, 111))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(LoadGamePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(SaveGamePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(NewGamePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(25, 25, 25)
                .addComponent(CreateOrganizmPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
       String wx=jTextField1.getText();
        if (isNumeric(wx)){
            if (Integer.parseInt(wx)>9 && Integer.parseInt(wx)<31){
                wartoscx=Integer.parseInt(wx);
            }
            else {
                textArea.append("Rozmiar X musi być większy niż 9 ale mniejszy niz 31!\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
            }
        }
        else{
            textArea.append("Wartość X nie jest liczbą!\n");
            textArea.setCaretPosition(textArea.getDocument().getLength());
        }
        jTextField2.requestFocusInWindow();
        jTextField1.requestFocusInWindow();
        jButton1.requestFocusInWindow();
        textArea.setFocusable(false);
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        String wx=jTextField2.getText();
        if (isNumeric(wx)){
            if (Integer.parseInt(wx)>9 && Integer.parseInt(wx)<31){
                wartoscy=Integer.parseInt(wx);
            }
            else{
                textArea.append("Rozmiar Y musi być większy niż 9 ale mniejszy niz 30!\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
            }
        }
        else{
            textArea.append("Wartość Y nie jest liczbą!\n");
            textArea.setCaretPosition(textArea.getDocument().getLength());
        }
    jTextField2.requestFocusInWindow();
        jTextField1.requestFocusInWindow();
        jButton1.requestFocusInWindow();
        textArea.setFocusable(false);
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (wartoscx==0 || wartoscy==0){
            textArea.append("Wypełniłeś źle pola!\n");
            textArea.setCaretPosition(textArea.getDocument().getLength());
            jTextField2.requestFocusInWindow();
            jTextField1.requestFocusInWindow();
            jButton1.requestFocusInWindow();
            textArea.setFocusable(false);
        }
        else{
            pomFrame.dispose();
            pomFrame=null;
            window.dispose();
            gui=null;
            message=null;
            kolejnosc=null;
            organizmy=null;
            ButtonsXY=null;
            MapaXY=null;
            Rightpanel=null;
            window=null;
            message=null;
            tools=null;
            listPane=null;
            textArea=null;
            NewGame=null;
            size_active=0;
            blokada=false;
            gui = new Gui(this);
            message = new JLabel("Witaj w mojej grze!");
            max_x =wartoscx;
            max_y = wartoscy;
            wartoscx=0;
            wartoscy=0;
            blokada=false;
            kolejnosc = new Organizm[max_y*max_x];
            organizmy = new Organizm[max_y*max_x];
            ButtonsXY = new JButton[max_y][max_x];
            size_active = 0;
            dodajOrganizmy();
            initializeGui();
            ustawSwiat();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        zapiszGre();
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        wczytajGre();
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        stworzOrganizm();
        
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel CreateOrganizmPanel;
    private javax.swing.JList JListOrganizmy;
    private javax.swing.JPanel LoadGamePanel;
    private javax.swing.JPanel NewGamePanel;
    private javax.swing.JPanel SaveGamePanel;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projektpo;

/**
 *
 * @author MychauU
 */
import organizm.Organizm;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.*;
import static javax.swing.Action.ACTION_COMMAND_KEY;

@SuppressWarnings("serial")
public class Gui extends JPanel {
    Swiat pochodzenie;
   public Gui(Swiat prs) {
      pochodzenie=prs;
      ActionMap actionMap = getActionMap();
      int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
      InputMap inputMap = getInputMap(condition);
      for (Direction direction : Direction.values()) {
         inputMap.put(direction.getKeyStroke(), direction.getText());
         actionMap.put(direction.getText(), new MyArrowBinding(direction.getText()));
      }
   }

   private class MyArrowBinding extends AbstractAction {
      public MyArrowBinding(String text) {
         super(text);
         putValue(ACTION_COMMAND_KEY, text);
      }

      @Override
      public void actionPerformed(ActionEvent e) {
         if (pochodzenie.blokada!=true){
            String actionCommand = e.getActionCommand();
            Organizm czlowiek=pochodzenie.seekHuman();
            if (czlowiek!=null){
                czlowiek.setRuch(actionCommand);
              //  System.out.println(actionCommand);
                pochodzenie.wykonajTure();
            }
            else{
                pochodzenie.textArea.append("ZGINĄŁEŚ! KONIEC GRY\n");
                pochodzenie.textArea.setCaretPosition(pochodzenie.textArea.getDocument().getLength());
            }
         }
        
         
      }

   }

}

enum Direction {
   UP("Up", KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0)),
   DOWN("Down", KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0)),
   LEFT("Left", KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0)),
   RIGHT("Right", KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0)),
   POWER("Power", KeyStroke.getKeyStroke(KeyEvent.VK_P, 0));
   Direction(String text, KeyStroke keyStroke) {
      this.text = text;
      this.keyStroke = keyStroke;
   }
   private String text;
   private KeyStroke keyStroke;

   public String getText() {
      return text;
   }

   public KeyStroke getKeyStroke() {
      return keyStroke;
   }

   @Override
   public String toString() {
      return text;
   }
}

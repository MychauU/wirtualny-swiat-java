/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rosliny;

import projektpo.Swiat;
import rosliny.Roslina;

/**
 *
 * @author MychauU
 */
public class Trawa extends Roslina{
    public Trawa(int xx, int yy, Swiat swiat){
        super(xx,yy,100,0,0,"Trawa",1,swiat);
    }
    @Override
    public char rysowanie(){
        return 'm';
    }
    
    @Override
    public void akcja(){
        int wiekk=getWiek();
     if (wiekk >= 0 && wiekk <= 35)
            super.akcja();
	setWiek(wiekk+3);
    }
    
    
}

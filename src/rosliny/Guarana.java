/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rosliny;

import rosliny.Roslina;
import organizm.Organizm;
import projektpo.Swiat;
import projektpo.Swiat.Komunikaty;

/**
 *
 * @author MychauU
 */
public class Guarana extends Roslina {
    public Guarana(int xx, int yy, Swiat swiat){
        super(xx,yy,102,0,0,"Guarana",1,swiat);
    }
    public char rysowanie(){
        return '{';
    }
    public int kolizja(Organizm kolizujacy){
        kolizujacy.setSila(getSila()+3);
        return Komunikaty.ZABIJ.getVal();
    }
    public void akcja(){
        int wiekk=getWiek();
        if (wiekk >= 0 && wiekk <= 35)
            super.akcja();
	setWiek(wiekk+3);
    }
}

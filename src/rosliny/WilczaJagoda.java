/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rosliny;

import projektpo.Swiat;
import zwierzeta.Zwierze;
import rosliny.Roslina;

/**
 *
 * @author MychauU
 */
public class WilczaJagoda extends Roslina {
    public WilczaJagoda(int xx, int yy, Swiat swiat){
        super(xx,yy,100,0,0,"Wilcza jagoda",1,swiat);
    }
    @Override
    public char rysowanie(){
        return '#';
    }
    @Override
    public int kolizja(Zwierze kolizujacy){
        return Swiat.Komunikaty.ZUM.getVal();
    }
    @Override
    public void akcja(){
        int wiekk=getWiek();
        if (wiekk >= 0 && wiekk <= 35)
            super.akcja();
	setWiek(wiekk+3);
    }
}

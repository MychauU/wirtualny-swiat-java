/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rosliny;

import projektpo.Swiat;
import rosliny.Roslina;

/**
 *
 * @author MychauU
 */
public class Mlecz extends Roslina{
    public Mlecz(int xx, int yy, Swiat swiat){
        super(xx,yy,100,0,0,"Mlecz",1,swiat);
    }
    @Override
    public char rysowanie(){
        return '*';
    }
    @Override
    public void akcja(){
        int wiekk=getWiek();
        for (int i = 0; i < 3; i++){
            if (wiekk >= 7 && wiekk <= 36)
                super.akcja();
        }
        setWiek(wiekk+5);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rosliny;

import organizm.Organizm;
import java.util.Random;
import javax.swing.JTextArea;
import projektpo.Swiat;
import static projektpo.Swiat.Komunikaty;
import projektpo.Swiat.Tabela_organizmow;
import zwierzeta.Zwierze;
/**
 *
 * @author MychauU
 */
public abstract class Roslina extends Organizm{
    
    Roslina(int xx, int yy, int idd, int silaa,int inicjatywaa,String wyraz, int wiekk, Swiat swiat){
        super(xx,yy,idd,silaa,inicjatywaa,wyraz,wiekk,swiat);
}
    @Override
    public int kolizja(Zwierze kolizujacy){
        return Komunikaty.ZABIJ.getVal();
    }
    @Override
    @SuppressWarnings("empty-statement")
    public void akcja(){
        JTextArea textArea=pochodzenie.getTextArea();
        int max_x = pochodzenie.getXsize();
        int max_y = pochodzenie.getYsize();
        Organizm []organizmy = pochodzenie.getOrg();
        int proba;
        char []wynik = new char[9];
        Random generator=new Random();
        proba = generator.nextInt(1000)+1;
        
        if (proba >= 200 && proba <= 300) {
            proba = 0;
            int xpom=getX();
            int ypom=getY();
            if (xpom > 0 && ypom  > 0){
                if (organizmy[xpom - 1 + (ypom - 1)*max_x] == null){
                    wynik[proba] = '1';
                    proba++;
                }
            }
            if (ypom > 0){
                if ((organizmy[xpom + (ypom - 1)*max_x] == null)){
                    wynik[proba] = '2';
                    proba++;
                }
            }
            if (xpom<max_x-1 && ypom > 0){
                if ((organizmy[xpom + 1 + (ypom - 1)*max_x] == null)){
                    wynik[proba] = '3';
                    proba++;
                }
            }
            if (xpom> 0){
                if ((organizmy[xpom - 1 + (ypom)*max_x] == null)){
                    wynik[proba] = '4';
                    proba++;
                }
            }
            if (xpom < max_x -1){
                if ((organizmy[xpom + 1 + (ypom)*max_x] == null)){
                    wynik[proba] = '5';
                    proba++;
                    }
            }
            if (xpom  >0 && ypom < max_y-1){
                if ((organizmy[xpom - 1 + (ypom + 1)*max_x] == null)){
                    wynik[proba] = '6';
                    proba++;
                    }
            }
            if (ypom< max_y - 1){
                if ((organizmy[xpom + (ypom + 1)*max_x] == null)){
                    wynik[proba] = '7';
                    proba++;
                    }
            }
            if (xpom < max_x - 1 && ypom  < max_y - 1){
                if ((organizmy[xpom +1+ (ypom + 1)*max_x] == null)){
                    wynik[proba] = '8';
                    proba++; 
                    }
            }
            if (proba == 0) wynik[0] = 0;
            if (wynik[0] != 0){
                int TiD=getId();
                for (proba = 1; wynik[proba] != 0; proba++);
                proba = generator.nextInt(proba);
                textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" rozsiała się\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());
                switch (wynik[proba]){
                    case '1':{
                        if (TiD == Tabela_organizmow.TRAWA.getVal()){
                            organizmy[xpom - 1 + (ypom - 1)*max_x] = new  Trawa(xpom - 1, ypom - 1, pochodzenie);
                        }
                        else if (TiD == Tabela_organizmow.MLECZ.getVal()) organizmy[xpom - 1 + (ypom - 1)*max_x] = new  Mlecz(xpom - 1, ypom - 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.GUARANA.getVal()) organizmy[xpom - 1 + (ypom - 1)*max_x] = new  Guarana(xpom - 1, ypom - 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.WILCZAJAGODA.getVal()) organizmy[xpom - 1 + (ypom - 1)*max_x] = new  WilczaJagoda(xpom - 1, ypom - 1, pochodzenie);
                    }
                    break;
                    case '2':{
                        if (TiD == Tabela_organizmow.TRAWA.getVal()) {
                            organizmy[xpom  + (ypom - 1)*max_x] = new  Trawa(xpom , ypom - 1, pochodzenie);
                        }
                        else if (TiD == Tabela_organizmow.MLECZ.getVal()) organizmy[xpom + (ypom - 1)*max_x] = new  Mlecz(xpom, ypom - 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.GUARANA.getVal()) organizmy[xpom + (ypom - 1)*max_x] = new  Guarana(xpom, ypom - 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.WILCZAJAGODA.getVal()) organizmy[xpom + (ypom - 1)*max_x] = new  WilczaJagoda(xpom, ypom - 1, pochodzenie);

                    }
                    break;
                    case '3':{
                        if (TiD == Tabela_organizmow.TRAWA.getVal()){
                            organizmy[xpom + 1 + (ypom - 1)*max_x] = new  Trawa(xpom + 1, ypom - 1, pochodzenie);
                        }
                        else if (TiD == Tabela_organizmow.MLECZ.getVal()) organizmy[xpom + 1 + (ypom - 1)*max_x] = new  Mlecz(xpom + 1, ypom - 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.GUARANA.getVal()) organizmy[xpom + 1 + (ypom - 1)*max_x] = new  Guarana(xpom + 1, ypom - 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.WILCZAJAGODA.getVal()) organizmy[xpom + 1 + (ypom - 1)*max_x] = new  WilczaJagoda(xpom + 1, ypom - 1, pochodzenie);
                    }
                    break;
                    case '4':{
                        if (TiD == Tabela_organizmow.TRAWA.getVal()){
                            organizmy[xpom - 1 + (ypom)*max_x] = new  Trawa(xpom - 1, ypom, pochodzenie);
                        }
                        else if (TiD == Tabela_organizmow.MLECZ.getVal()) organizmy[xpom - 1 + (ypom)*max_x] = new  Mlecz(xpom - 1, ypom, pochodzenie);
                        else if (TiD == Tabela_organizmow.GUARANA.getVal()) organizmy[xpom - 1 + (ypom)*max_x] = new  Guarana(xpom - 1, ypom, pochodzenie);
                        else if (TiD == Tabela_organizmow.WILCZAJAGODA.getVal()) organizmy[xpom - 1 + (ypom)*max_x] = new  WilczaJagoda(xpom - 1, ypom, pochodzenie);
                    }
                    break;
                    case '5':{
                        if (TiD == Tabela_organizmow.TRAWA.getVal()){
                            organizmy[xpom + 1 + (ypom)*max_x] = new  Trawa(xpom + 1, ypom, pochodzenie);
                        }
                        else if (TiD == Tabela_organizmow.MLECZ.getVal()) organizmy[xpom +1 + (ypom)*max_x] = new  Mlecz(xpom + 1, ypom, pochodzenie);
                        else if (TiD == Tabela_organizmow.GUARANA.getVal()) organizmy[xpom + 1 + (ypom)*max_x] = new  Guarana(xpom + 1, ypom, pochodzenie);
                        else if (TiD == Tabela_organizmow.WILCZAJAGODA.getVal()) organizmy[xpom + 1 + (ypom)*max_x] = new  WilczaJagoda(xpom + 1, ypom, pochodzenie);
                    }
                    break;
                    case '6':{
                        if (TiD == Tabela_organizmow.TRAWA.getVal()){
                            organizmy[xpom - 1 + (ypom + 1)*max_x] = new  Trawa(xpom - 1, ypom + 1, pochodzenie);
                        }
                        else if (TiD == Tabela_organizmow.MLECZ.getVal()) organizmy[xpom - 1 + (ypom + 1)*max_x] = new  Mlecz(xpom - 1, ypom + 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.GUARANA.getVal()) organizmy[xpom - 1 + (ypom + 1)*max_x] = new  Guarana(xpom - 1, ypom + 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.WILCZAJAGODA.getVal()) organizmy[xpom - 1 + (ypom + 1)*max_x] = new  WilczaJagoda(xpom - 1, ypom + 1, pochodzenie);
                    }
                    break;
                    case '7':{
                        if (TiD == Tabela_organizmow.TRAWA.getVal()) {
                            organizmy[xpom + (ypom + 1)*max_x] = new  Trawa(xpom , ypom + 1, pochodzenie);
                        }
                        else if (TiD == Tabela_organizmow.MLECZ.getVal()) organizmy[xpom + (ypom + 1)*max_x] = new  Mlecz(xpom, ypom + 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.GUARANA.getVal()) organizmy[xpom + (ypom + 1)*max_x] = new  Guarana(xpom, ypom + 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.WILCZAJAGODA.getVal()) organizmy[xpom + (ypom + 1)*max_x] = new  WilczaJagoda(xpom, ypom + 1, pochodzenie);
                    }
                    break;
                    case '8':{
                        if (TiD == Tabela_organizmow.TRAWA.getVal()) {
                            organizmy[xpom + 1 + (ypom + 1)*max_x] = new  Trawa(xpom + 1, ypom + 1, pochodzenie);
                        }
                        else if (TiD == Tabela_organizmow.MLECZ.getVal()) organizmy[xpom + 1 + (ypom + 1)*max_x] = new  Mlecz(xpom + 1, ypom + 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.GUARANA.getVal()) organizmy[xpom + 1 + (ypom + 1)*max_x] = new  Guarana(xpom + 1, ypom + 1, pochodzenie);
                        else if (TiD == Tabela_organizmow.WILCZAJAGODA.getVal()) organizmy[xpom + 1 + (ypom + 1)*max_x] = new  WilczaJagoda(xpom + 1, ypom + 1, pochodzenie);
                    }
                    break;
                }
            }
        }
    }
}

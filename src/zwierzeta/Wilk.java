/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zwierzeta;

import projektpo.Swiat;
import zwierzeta.Zwierze;

/**
 *
 * @author MychauU
 */
public class Wilk extends Zwierze{
    @Override
    public char rysowanie(){
        return 'W';
    }
    public Wilk(int xx, int yy, Swiat swiat){
        super(xx, yy, 201, 9, 5, "Wilk", 1, swiat);
    }
}

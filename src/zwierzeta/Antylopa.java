/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zwierzeta;

import zwierzeta.Zwierze;
import organizm.Organizm;
import java.util.Random;
import javax.swing.JTextArea;
import projektpo.Swiat;
import projektpo.Swiat.Komunikaty;
import projektpo.Swiat.Tabela_organizmow;
/**
 *
 * @author MychauU
 */
public class Antylopa extends Zwierze {
    public Antylopa(int xx, int yy, Swiat swiat){
        super(xx, yy, 205, 4, 4, "Antylopa", 1, swiat);
    }
    @Override
    public char rysowanie(){
		return 'A';
    }
    @Override
    @SuppressWarnings("empty-statement")
    public int kolizja(Zwierze kolizujacy){
        JTextArea textArea=pochodzenie.getTextArea();
		int max_x = pochodzenie.getXsize();
		 int max_y = pochodzenie.getYsize();
		Organizm []organizmy = pochodzenie.getOrg();
		if (kolizujacy.getId() == this.getId()){
			if (kolizujacy.getZmeczony() == 0 && this.getZmeczony() == 0){
				this.setZmeczony(6);
				return Komunikaty.ROZMNAZAJ.getVal();
			}
			else return Komunikaty.NIC.getVal();
		}
		else if (kolizujacy.getSila() >= this.getSila()){
                        Random generator=new Random();
			int pom = generator.nextInt(2);
                        int xpom=getX();
                        int ypom=getY();
			if (pom == 1) return Komunikaty.ZABIJ.getVal();
			else {
				char []wynik = new char[9];
				int proba = 0;
				if (xpom > 0 && ypom > 0){
					if (organizmy[xpom - 1 + (ypom - 1)*max_x] == null){
						wynik[proba] = '1';
						proba++;
					}
				}
				if (ypom > 0){
					if ((organizmy[xpom + (ypom - 1)*max_x] == null)){
						wynik[proba] = '2';
						proba++;
					}
				}
				if (xpom < max_x - 1 && ypom > 0){
					if ((organizmy[xpom + 1 + (ypom - 1)*max_x] == null)){
						wynik[proba] = '3';
						proba++;
					}
				}
				if (xpom > 0){
					if ((organizmy[xpom - 1 + (ypom)*max_x] == null)){
						wynik[proba] = '4';
						proba++;
					}
				}
				if (xpom < max_x - 1){
					if ((organizmy[xpom + 1 + (ypom)*max_x] == null)){
						wynik[proba] = '5';
						proba++;
					}
				}
				if (xpom > 0 && ypom < max_y - 1){
					if ((organizmy[xpom - 1 + (ypom + 1)*max_x] == null)){
						wynik[proba] = '6';
						proba++;
					}
				}
				if (ypom < max_y - 1){
					if ((organizmy[xpom + (ypom + 1)*max_x] == null)){
						wynik[proba] = '7';
						proba++;
					}
				}
				if (xpom < max_x - 1 && ypom < max_y - 1){
					if ((organizmy[xpom +1+ (ypom + 1)*max_x] == null)){
						wynik[proba] = '8';
						proba++;
					}
				}
				if (proba == 0) wynik[0] = 0;
				if (wynik[0] != 0){
					for (proba = 1; wynik[proba] != 0; proba++);
					proba = generator.nextInt(proba);
					switch (wynik[proba]){
					case '1':{
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom -1+ (ypom - 1)*max_x);
						setY(ypom-1);
                                                setX(xpom-1);
					}
						break;
					case '2':{
						pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x);
						setY(ypom-1);
					}
						break;
					case '3':{
						pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom+1 + (ypom - 1)*max_x);
						setY(ypom-1);
						setX(xpom+1);
					}
						break;
					case '4':{
						pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom -1+ (ypom)*max_x);
						setX(xpom-1);
					}
						break;
					case '5':{
						pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x);
						setX(xpom+1);
					}
						break;
					case '6':{
						pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom+1)*max_x);
						setX(xpom-1);
						setY(ypom+1);
					}
						break;
					case '7':{
						pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom  + (ypom+1)*max_x);
						setY(ypom+1);
					}
						break;
					case '8':{
						pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom+1)*max_x);
						setX(xpom+1);
						setY(ypom+1);
					}
						break;
					}
                                        textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" uciekł z walki \n");
                                        textArea.setCaretPosition(textArea.getDocument().getLength());
					return Komunikaty.UCIEKL.getVal();
				}
				else return Komunikaty.ZABIJ.getVal();
			}
		}
		else if (kolizujacy.getSila() < this.getSila()){
			return Komunikaty.UMIERAJ.getVal();
		}
                else return Komunikaty.NIC.getVal();
	}
    @Override
    @SuppressWarnings("empty-statement")
    public void akcja(){
		int max_x = pochodzenie.getXsize();
		int max_y = pochodzenie.getYsize();
		Organizm []organizmy = pochodzenie.getOrg();
		Organizm []kolejnosc = pochodzenie.getKol();
		int size_active = pochodzenie.getSizeOfHeap();
                JTextArea textArea=pochodzenie.getTextArea();
		int ruch;
		int zdarzenie;
                int xpom=getX();
                int ypom=getY();
                Random generator=new Random();
		ruch = generator.nextInt(4);
		switch (ruch){
		case 0:{
			if (ypom > 1){
				if ((organizmy[xpom + (ypom - 2)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 2)*max_x);
					setY(ypom-2);
				}
				else {
					zdarzenie = organizmy[xpom + (ypom - 2)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 2)*max_x);
						setY(ypom-2);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom - 2)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom - 2)*max_x].getId()>=100 && organizmy[xpom + (ypom - 2)*max_x].getId()<200) || organizmy[xpom + (ypom - 2)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom - 2)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 2)*max_x].getX()+" i "+organizmy[xpom + (ypom - 2)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom - 2)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 2)*max_x].getX()+" i "+organizmy[xpom + (ypom - 2)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());                   
						organizmy[xpom + (ypom - 2)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 2)*max_x);
						setY(ypom-2);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom - 2)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom - 2)*max_x].getId()>=100 && organizmy[xpom + (ypom - 2)*max_x].getId()<200) || organizmy[xpom + (ypom - 2)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom - 2)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 2)*max_x].getX()+" i "+organizmy[xpom + (ypom - 2)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom - 2)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 2)*max_x].getX()+" i "+organizmy[xpom + (ypom - 2)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength()); 
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom + (ypom - 2)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());
					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal()){
						char []wynik = new char[9];
						int proba = 0;
						if (xpom > 0 && ypom > 0){
							if (organizmy[xpom - 1 + (ypom - 1)*max_x] == null){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (ypom > 0){
							if ((organizmy[xpom + (ypom - 1)*max_x] == null)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom > 0){
							if ((organizmy[xpom + 1 + (ypom - 1)*max_x] == null)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = 0;
						if (wynik[0] != 0){
							for (proba = 1; wynik[proba] != 0; proba++);
							proba =generator.nextInt(proba);
                                                        int TiD=getId();
                                                        textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" rozmnaża się\n");
                                                        textArea.setCaretPosition(textArea.getDocument().getLength());
							switch (wynik[proba]){
							case '1':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom - 1 + (ypom - 1)*max_x] = new  Antylopa(xpom - 1, ypom - 1, pochodzenie);
							}
								break;
							case '2':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom + (ypom - 1)*max_x] = new  Antylopa(xpom, ypom - 1, pochodzenie);
							}
								break;
							case '3':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom + 1 + (ypom - 1)*max_x] = new  Antylopa(xpom + 1, ypom - 1, pochodzenie);
							}
								break;
							}
							setZmeczony(10);
						}
					}
				}
			}
		}
			break;
		case 1:{
			if (ypom <max_y - 2){
				if ((organizmy[xpom + (ypom + 2)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 2)*max_x);
					setY(ypom+2);
				}
				else {
					zdarzenie = organizmy[xpom + (ypom + 2)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 2)*max_x);
						setY(ypom+2);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom +2)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom + 2)*max_x].getId()>=100 && organizmy[xpom + (ypom + 2)*max_x].getId()<200) || organizmy[xpom + (ypom + 2)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom + 2)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 2)*max_x].getX()+" i "+organizmy[xpom + (ypom + 2)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom + 2)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 2)*max_x].getX()+" i "+organizmy[xpom + (ypom + 2)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom + (ypom + 2)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 2)*max_x);
						setY(ypom+2);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
						organizmy[xpom + (ypom)*max_x] = null;
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						return;
					}
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom +2)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom + 2)*max_x].getId()>=100 && organizmy[xpom + (ypom + 2)*max_x].getId()<200) || organizmy[xpom + (ypom + 2)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom + 2)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 2)*max_x].getX()+" i "+organizmy[xpom + (ypom + 2)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom + 2)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 2)*max_x].getX()+" i "+organizmy[xpom + (ypom + 2)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom + (ypom + 2)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());
					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal()){
						char []wynik = new char[9];
						int proba = 0;
						if (ypom< max_y - 1){
							if ((organizmy[xpom + (ypom + 1)*max_x] == null)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (xpom > 0 && ypom < max_y - 1){
							if ((organizmy[xpom - 1 + (ypom + 1)*max_x] == null)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom < max_y - 1){
							if ((organizmy[xpom +1 +(ypom + 1)*max_x] == null)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = 0;
						if (wynik[0] != 0){
							for (proba = 1; wynik[proba] != 0; proba++);
							proba =generator.nextInt(proba);
                                                        int TiD=getId();
                                                        textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" rozmnaża się\n");
                                                        textArea.setCaretPosition(textArea.getDocument().getLength());
							switch (wynik[proba]){
							case '6':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom - 1 + (ypom + 1)*max_x] = new  Antylopa(xpom - 1, ypom + 1, pochodzenie);
							}
								break;
							case '7':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom + (ypom + 1)*max_x] = new  Antylopa(xpom, ypom + 1, pochodzenie);
							}
								break;
							case '8':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom + 1 + (ypom + 1)*max_x] = new  Antylopa(xpom + 1, ypom + 1, pochodzenie);
							}
								break;
							}
							setZmeczony(10);
						}
					}
				}

			}
		}
			break;
		case 2:{
			if (xpom >1){
				if ((organizmy[xpom - 2 + (ypom)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 2 + (ypom)*max_x);
					setX(xpom-2);
				}
				else {
					zdarzenie = organizmy[xpom - 2 + (ypom)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 2 + (ypom)*max_x);
						setX(xpom-2);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom-2 + (ypom)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom -2+ (ypom)*max_x].getId()>=100 && organizmy[xpom -1+ (ypom)*max_x].getId()<200) || organizmy[xpom -1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom -2+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -2+ (ypom)*max_x].getX()+" i "+organizmy[xpom -2+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom -2+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -2+ (ypom)*max_x].getX()+" i "+organizmy[xpom -2+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom - 2 + (ypom)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 2 + (ypom)*max_x);
						setX(xpom-2);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
						organizmy[xpom + (ypom)*max_x] = null;
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						return;
					}
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom -2+ (ypom)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom -2+ (ypom)*max_x].getId()>=100 && organizmy[xpom -1+ (ypom)*max_x].getId()<200) || organizmy[xpom -1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom -2+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -2+ (ypom)*max_x].getX()+" i "+organizmy[xpom -2+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom -2+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -2+ (ypom)*max_x].getX()+" i "+organizmy[xpom -2+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom - 2 + (ypom)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());
					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal()){
						char []wynik = new char[9];
						int proba = 0;
						if (xpom > 0 && ypom > 0){
							if (organizmy[xpom - 1 + (ypom - 1)*max_x] == null){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (xpom > 0){
							if ((organizmy[xpom - 1 + (ypom)*max_x] == null)){
								wynik[proba] = '4';
								proba++;
							}
						}
						if (xpom > 0 && ypom < max_y - 1){
							if ((organizmy[xpom - 1 + (ypom + 1)*max_x] == null)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = 0;
						if (wynik[0] != 0){
							for (proba = 1; wynik[proba] != 0; proba++);
							proba =generator.nextInt(proba);
                                                        int TiD=getId();
                                                        textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" rozmnaża się\n");
                                                        textArea.setCaretPosition(textArea.getDocument().getLength());
							switch (wynik[proba]){
							case '1':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom - 1 + (ypom - 1)*max_x] = new  Antylopa(xpom - 1, ypom - 1, pochodzenie);
							}
								break;
							case '4':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom - 1 + (ypom)*max_x] = new  Antylopa(xpom - 1, ypom, pochodzenie);
							}
								break;
							case '6':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom - 1 + (ypom + 1)*max_x] = new  Antylopa(xpom - 1, ypom + 1, pochodzenie);
							}
								break;
							}
							setZmeczony(10);
						}
					}
				}

			}
		}
			break;
		case 3:{
			if (xpom <max_x - 2){
				if ((organizmy[xpom + 2 + (ypom)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 2 + (ypom)*max_x);
					setX(xpom+2);
				}
				else {
					zdarzenie = organizmy[xpom + 2 + (ypom)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 2 + (ypom)*max_x);
						setX(xpom+2);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom +2+ (ypom )*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom +2+ (ypom)*max_x].getId()>=100 && organizmy[xpom +2+ (ypom)*max_x].getId()<200) || organizmy[xpom +2+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom +2+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +2+ (ypom)*max_x].getX()+" i "+organizmy[xpom +2+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom +2+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +2+ (ypom)*max_x].getX()+" i "+organizmy[xpom +2+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom + 2 + (ypom)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 2 + (ypom)*max_x);
						setX(xpom+2);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
						organizmy[xpom + (ypom)*max_x] = null;
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						return;
					}
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom +2+ (ypom)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom +2+ (ypom)*max_x].getId()>=100 && organizmy[xpom +2+ (ypom)*max_x].getId()<200) || organizmy[xpom +2+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom +2+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +2+ (ypom)*max_x].getX()+" i "+organizmy[xpom +2+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom +2+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +2+ (ypom)*max_x].getX()+" i "+organizmy[xpom +2+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom + 2 + (ypom)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());

					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal()){
						char []wynik = new char[9];
						int proba = 0;
						if (xpom < max_x - 1 && ypom > 0){
							if ((organizmy[xpom + 1 + (ypom - 1)*max_x] == null)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (xpom < max_x - 1){
							if ((organizmy[xpom + 1 + (ypom)*max_x] == null)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom < max_y - 1){
							if ((organizmy[xpom +1+ (ypom + 1)*max_x] == null)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = 0;
						if (wynik[0] != 0){
							for (proba = 1; wynik[proba] != 0; proba++);
							proba =generator.nextInt(proba);
                                                        int TiD=getId();
                                                        textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" rozmnaża się\n");
                                                        textArea.setCaretPosition(textArea.getDocument().getLength());
							switch (wynik[proba]){
							case '3':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom + 1 + (ypom - 1)*max_x] = new  Antylopa(xpom + 1, ypom - 1, pochodzenie);
							}
								break;
							case '5':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom + 1 + (ypom)*max_x] = new  Antylopa(xpom + 1, ypom, pochodzenie);
							}
								break;
							case '8':{
								if (TiD == Tabela_organizmow.ANTYLOPA.getVal()) organizmy[xpom + 1 + (ypom + 1)*max_x] = new  Antylopa(xpom + 1, ypom + 1, pochodzenie);
							}
								break;
							}
							setZmeczony(10);
						}
					}
				}

			}
		}
			break;
		}
		if (getZmeczony()>0)
			setZmeczony(getZmeczony()-1);
		setWiek(getWiek()+1);
	}
}

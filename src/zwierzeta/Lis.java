/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zwierzeta;

import zwierzeta.Zwierze;
import organizm.Organizm;
import java.util.Random;
import javax.swing.JTextArea;
import projektpo.Swiat;
import projektpo.Swiat.Komunikaty;
import projektpo.Swiat.Tabela_organizmow;

/**
 *
 * @author MychauU
 */
public class Lis extends Zwierze {
    public Lis(int xx, int yy, Swiat swiat){
        super(xx, yy, 203, 3, 7, "Lis", 1, swiat);
    }
    @Override
    public char rysowanie(){
		return 'F';
    }
    @Override
    @SuppressWarnings("empty-statement")
    public void akcja(){
		int max_x = pochodzenie.getXsize();
		int max_y = pochodzenie.getYsize();
		Organizm []organizmy = pochodzenie.getOrg();
		Organizm []kolejnosc = pochodzenie.getKol();
		int size_active = pochodzenie.getSizeOfHeap();
                JTextArea textArea=pochodzenie.getTextArea();
		int ruch;
		int zdarzenie;
		int xpom=getX();
                int ypom=getY();
                Random generator=new Random();
		ruch = generator.nextInt(4);
		switch (ruch){
		case 0:{
			if (ypom > 0){
				if ((organizmy[xpom + (ypom - 1)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x);
					setY(ypom-1);
				}
				else {
					zdarzenie = organizmy[xpom + (ypom - 1)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x);
						setY(ypom-1);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom - 1)*max_x].getId()>=100 && organizmy[xpom + (ypom - 1)*max_x].getId()<200) || organizmy[xpom + (ypom - 1)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom - 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 1)*max_x].getX()+" i "+organizmy[xpom + (ypom - 1)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom - 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 1)*max_x].getX()+" i "+organizmy[xpom + (ypom - 1)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom + (ypom - 1)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x);
						setY(ypom-1);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
                                            textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" nie atakuje silniejszego przeciwnika\n");
                                            textArea.setCaretPosition(textArea.getDocument().getLength());  
                                            return;
                                        }
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom - 1)*max_x].getId()>=100 && organizmy[xpom + (ypom - 1)*max_x].getId()<200) || organizmy[xpom + (ypom - 1)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom - 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 1)*max_x].getX()+" i "+organizmy[xpom + (ypom - 1)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom - 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 1)*max_x].getX()+" i "+organizmy[xpom + (ypom - 1)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());    
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength()); 
						organizmy[xpom + (ypom - 1)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());
					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal()){
						char []wynik = new char[9];
						int proba = 0;
						if (xpom > 0 && ypom > 0){
							if (organizmy[xpom - 1 + (ypom - 1)*max_x] == null){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (ypom > 0){
							if ((organizmy[xpom + (ypom - 1)*max_x] == null)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom > 0){
							if ((organizmy[xpom + 1 + (ypom - 1)*max_x] == null)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (xpom > 0){
							if ((organizmy[xpom - 1 + (ypom)*max_x] == null)){
								wynik[proba] = '4';
								proba++;
							}
						}
						if (xpom < max_x - 1){
							if ((organizmy[xpom + 1 + (ypom)*max_x] == null)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (xpom > 0 && ypom < max_y - 1){
							if ((organizmy[xpom - 1 + (ypom + 1)*max_x] == null)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (ypom < max_y - 1){
							if ((organizmy[xpom + (ypom + 1)*max_x] == null)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom < max_y - 1){
							if ((organizmy[xpom +1+ (ypom + 1)*max_x] == null)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = 0;
						if (wynik[0] != 0){
							for (proba = 1; wynik[proba] != 0; proba++);
							proba = generator.nextInt(proba);
                                                        int TiD=getId();
                                                        textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" rozmnaża się\n");
                                                        textArea.setCaretPosition(textArea.getDocument().getLength());
							switch (wynik[proba]){
							case '1':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom - 1)*max_x] = new  Lis(xpom - 1, ypom - 1, pochodzenie);
							}
								break;
							case '2':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + (ypom - 1)*max_x] = new  Lis(xpom, ypom - 1, pochodzenie);				
							}
								break;
							case '3':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom - 1)*max_x] = new  Lis(xpom + 1, ypom - 1, pochodzenie);
							}
								break;
							case '4':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom)*max_x] = new  Lis(xpom - 1, ypom, pochodzenie);
							}
								break;
							case '5':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom)*max_x] = new  Lis(xpom + 1, ypom, pochodzenie);
							}
								break;
							case '6':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom + 1)*max_x] = new  Lis(xpom - 1, ypom + 1, pochodzenie);
							}
								break;
							case '7':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + (ypom + 1)*max_x] = new  Lis(xpom, ypom + 1, pochodzenie);
							}
								break;
							case '8':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom + 1)*max_x] = new  Lis(xpom + 1, ypom + 1, pochodzenie);
							}
								break;
							}
							setZmeczony(10);
						}
					}
				}
			}
		}
			break;
		case 1:{
			if (ypom <max_y - 1){
				if ((organizmy[xpom + (ypom + 1)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x);
					setY(ypom+1);
				}
				else {
					zdarzenie = organizmy[xpom + (ypom + 1)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x);
						setY(ypom+1);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom + 1)*max_x].getId()>=100 && organizmy[xpom + (ypom + 1)*max_x].getId()<200) || organizmy[xpom + (ypom + 1)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom + 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 1)*max_x].getX()+" i "+organizmy[xpom + (ypom + 1)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom + 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 1)*max_x].getX()+" i "+organizmy[xpom + (ypom + 1)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom + (ypom + 1)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x);
						setY(ypom+1);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
                                            textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" nie atakuje silniejszego przeciwnika\n");
                                            textArea.setCaretPosition(textArea.getDocument().getLength());  
                                        }
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom + 1)*max_x].getId()>=100 && organizmy[xpom + (ypom + 1)*max_x].getId()<200) || organizmy[xpom + (ypom+ 1)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom + 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 1)*max_x].getX()+" i "+organizmy[xpom + (ypom + 1)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom + 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 1)*max_x].getX()+" i "+organizmy[xpom + (ypom + 1)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());    

                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom + (ypom + 1)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());
					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal()){
						char []wynik = new char[9];
						int proba = 0;
						if (xpom > 0 && ypom > 0){
							if (organizmy[xpom - 1 + (ypom - 1)*max_x] == null){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (ypom > 0){
							if ((organizmy[xpom + (ypom - 1)*max_x] == null)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom > 0){
							if ((organizmy[xpom + 1 + (ypom - 1)*max_x] == null)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (xpom > 0){
							if ((organizmy[xpom - 1 + (ypom)*max_x] == null)){
								wynik[proba] = '4';
								proba++;
							}
						}
						if (xpom < max_x - 1){
							if ((organizmy[xpom + 1 + (ypom)*max_x] == null)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (ypom< max_y - 1){
							if ((organizmy[xpom + (ypom + 1)*max_x] == null)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (xpom > 0 && ypom < max_y - 1){
							if ((organizmy[xpom - 1 + (ypom + 1)*max_x] == null)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom < max_y - 1){
							if ((organizmy[xpom +1+ (ypom + 1)*max_x] == null)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = 0;
						if (wynik[0] != 0){
							for (proba = 1; wynik[proba] != 0; proba++);
							proba = generator.nextInt(proba);
                                                        int TiD = getId();
                                                        textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" rozmnaża się\n");
                                                        textArea.setCaretPosition(textArea.getDocument().getLength());
							switch (wynik[proba]){
							case '1':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom - 1)*max_x] = new  Lis(xpom - 1, ypom - 1, pochodzenie);
							}
								break;
							case '2':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + (ypom - 1)*max_x] = new  Lis(xpom, ypom - 1, pochodzenie);
							}
								break;
							case '3':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom - 1)*max_x] = new  Lis(xpom + 1, ypom - 1, pochodzenie);
							}
								break;
							case '4':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom)*max_x] = new  Lis(xpom - 1, ypom, pochodzenie);
							}
								break;
							case '5':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom)*max_x] = new  Lis(xpom + 1, ypom, pochodzenie);
							}
								break;
							case '6':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom + 1)*max_x] = new  Lis(xpom - 1, ypom + 1, pochodzenie);
							}
								break;
							case '7':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + (ypom + 1)*max_x] = new  Lis(xpom, ypom + 1, pochodzenie);
							}
								break;
							case '8':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom + 1)*max_x] = new  Lis(xpom + 1, ypom + 1, pochodzenie);
							}
								break;
							}
							setZmeczony(10);
						}
					}
				}

			}
		}
			break;
		case 2:{
			if (xpom >0){
				if ((organizmy[xpom - 1 + (ypom)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x);
					setX(xpom-1);
				}
				else {
					zdarzenie = organizmy[xpom - 1 + (ypom)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x);
						setX(xpom-1);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom -1+ (ypom )*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom -1+ (ypom)*max_x].getId()>=100 && organizmy[xpom -1+ (ypom)*max_x].getId()<200) || organizmy[xpom -1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom -1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -1+ (ypom)*max_x].getX()+" i "+organizmy[xpom -1+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom -1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -1+ (ypom)*max_x].getX()+" i "+organizmy[xpom -1+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom - 1 + (ypom)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x);
						setX(xpom-1);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
                                            textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" nie atakuje silniejszego przeciwnika\n");
                                            textArea.setCaretPosition(textArea.getDocument().getLength());  
                                        }
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom -1+ (ypom )*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom -1+ (ypom)*max_x].getId()>=100 && organizmy[xpom -1+ (ypom)*max_x].getId()<200) || organizmy[xpom -1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom -1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -1+ (ypom)*max_x].getX()+" i "+organizmy[xpom -1+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom -1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -1+ (ypom)*max_x].getX()+" i "+organizmy[xpom -1+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
                                        	if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());
					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal()){
						char []wynik = new char[9];
						int proba = 0;
						if (xpom > 0 && ypom > 0){
							if (organizmy[xpom - 1 + (ypom - 1)*max_x] == null){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (ypom > 0){
							if ((organizmy[xpom + (ypom - 1)*max_x] == null)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom > 0){
							if ((organizmy[xpom + 1 + (ypom - 1)*max_x] == null)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (xpom > 0){
							if ((organizmy[xpom - 1 + (ypom)*max_x] == null)){
								wynik[proba] = '4';
								proba++;
							}
						}
						if (xpom < max_x - 1){
							if ((organizmy[xpom + 1 + (ypom)*max_x] == null)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (xpom > 0 && ypom < max_y - 1){
							if ((organizmy[xpom - 1 + (ypom + 1)*max_x] == null)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (ypom< max_y - 1){
							if ((organizmy[xpom + (ypom + 1)*max_x] == null)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom < max_y - 1){
							if ((organizmy[xpom + 1+(ypom + 1)*max_x] == null)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = 0;
						if (wynik[0] != 0){
							for (proba = 1; wynik[proba] != 0; proba++);
							proba = generator.nextInt(proba);
                                                        int TiD = getId();
                                                        textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" rozmnaża się\n");
                                                        textArea.setCaretPosition(textArea.getDocument().getLength());
							switch (wynik[proba]){
							case '1':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom - 1)*max_x] = new  Lis(xpom - 1, ypom - 1, pochodzenie);
							}
								break;
							case '2':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + (ypom - 1)*max_x] = new  Lis(xpom, ypom - 1, pochodzenie);
							}
								break;
							case '3':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom - 1)*max_x] = new  Lis(xpom + 1, ypom - 1, pochodzenie);
							}
								break;
							case '4':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom)*max_x] = new  Lis(xpom - 1, ypom, pochodzenie);
							}
								break;
							case '5':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom)*max_x] = new  Lis(xpom + 1, ypom, pochodzenie);
							}
								break;
							case '6':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom + 1)*max_x] = new  Lis(xpom - 1, ypom + 1, pochodzenie);
							}
								break;
							case '7':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + (ypom + 1)*max_x] = new  Lis(xpom, ypom + 1, pochodzenie);
							}
								break;
							case '8':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom + 1)*max_x] = new  Lis(xpom + 1, ypom + 1, pochodzenie);
							}
								break;
							}
							setZmeczony(10);
						}
					//	if (wynik) //delete  wynik;
					}
				}

			}
		}
			break;
		case 3:{
			if (xpom <max_x - 1){
				if ((organizmy[xpom + 1 + (ypom)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x);
					setX(xpom+1);
				}
				else {
					zdarzenie = organizmy[xpom + 1 + (ypom)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x);
						setX(xpom+1);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom +1+ (ypom)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
                                                }
                                                if ((organizmy[xpom +1+ (ypom)*max_x].getId()>=100 && organizmy[xpom +1+ (ypom)*max_x].getId()<200) || organizmy[xpom +1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom +1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +1+ (ypom)*max_x].getX()+" i "+organizmy[xpom +1+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom +1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +1+ (ypom)*max_x].getX()+" i "+organizmy[xpom +1+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom + 1 + (ypom)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x);
						setX(xpom+1);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
                                            textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" nie atakuje silniejszego przeciwnika\n");
                                            textArea.setCaretPosition(textArea.getDocument().getLength());  
                                        }
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom +1+ (ypom )*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom +1+ (ypom)*max_x].getId()>=100 && organizmy[xpom +1+ (ypom)*max_x].getId()<200) || organizmy[xpom +1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom +1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +1+ (ypom)*max_x].getX()+" i "+organizmy[xpom +1+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom +1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +1+ (ypom)*max_x].getX()+" i "+organizmy[xpom +1+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength()); 
						organizmy[xpom + 1 + (ypom)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());

					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal()){
						char []wynik = new char[9];
						int proba = 0;
						if (xpom > 0 && ypom > 0){
							if (organizmy[xpom - 1 + (ypom - 1)*max_x] == null){
								wynik[proba] = '1';
								proba++;
							}
						}
						if (ypom > 0){
							if ((organizmy[xpom + (ypom - 1)*max_x] == null)){
								wynik[proba] = '2';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom > 0){
							if ((organizmy[xpom + 1 + (ypom - 1)*max_x] == null)){
								wynik[proba] = '3';
								proba++;
							}
						}
						if (xpom > 0){
							if ((organizmy[xpom - 1 + (ypom)*max_x] == null)){
								wynik[proba] = '4';
								proba++;
							}
						}
						if (xpom < max_x - 1){
							if ((organizmy[xpom + 1 + (ypom)*max_x] == null)){
								wynik[proba] = '5';
								proba++;
							}
						}
						if (xpom > 0 && ypom < max_y - 1){
							if ((organizmy[xpom - 1 + (ypom + 1)*max_x] == null)){
								wynik[proba] = '6';
								proba++;
							}
						}
						if (ypom< max_y - 1){
							if ((organizmy[xpom + (ypom + 1)*max_x] == null)){
								wynik[proba] = '7';
								proba++;
							}
						}
						if (xpom < max_x - 1 && ypom < max_y - 1){
							if ((organizmy[xpom +1+ (ypom + 1)*max_x] == null)){
								wynik[proba] = '8';
								proba++;
							}
						}
						if (proba == 0) wynik[0] = 0;
						if (wynik[0] != 0){
							for (proba = 1; wynik[proba] != 0; proba++);
							proba = generator.nextInt(proba);
                                                        int TiD=getId();
                                                        textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" rozmnaża się\n");
                                                        textArea.setCaretPosition(textArea.getDocument().getLength());
							switch (wynik[proba]){
							case '1':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom - 1)*max_x] = new  Lis(xpom - 1, ypom - 1, pochodzenie);
							}
								break;
							case '2':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + (ypom - 1)*max_x] = new  Lis(xpom, ypom - 1, pochodzenie);
							}
								break;
							case '3':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom - 1)*max_x] = new  Lis(xpom + 1, ypom - 1, pochodzenie);
							}
								break;
							case '4':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom)*max_x] = new  Lis(xpom - 1, ypom, pochodzenie);
							}
								break;
							case '5':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom)*max_x] = new  Lis(xpom + 1, ypom, pochodzenie);
							}
								break;
							case '6':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom - 1 + (ypom + 1)*max_x] = new  Lis(xpom - 1, ypom + 1, pochodzenie);
							}
								break;
							case '7':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + (ypom + 1)*max_x] = new  Lis(xpom, ypom + 1, pochodzenie);
							}
								break;
							case '8':{
								if (TiD == Tabela_organizmow.LIS.getVal()) organizmy[xpom + 1 + (ypom + 1)*max_x] = new  Lis(xpom + 1, ypom + 1, pochodzenie);
							}
								break;
							}
							setZmeczony(10);
						}
					}
				}

			}
		}
			break;
		}
		if (getZmeczony()>0)
			setZmeczony(getZmeczony()-1);
		setWiek(getWiek()+1);
	}
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zwierzeta;

import zwierzeta.Zwierze;
import java.util.Random;
import javax.swing.JTextArea;
import projektpo.Swiat;
import projektpo.Swiat.Komunikaty;

/**
 *
 * @author MychauU
 */
public class Zolw extends Zwierze{
    @Override
    public char rysowanie(){
        return 'T';
    }
    public Zolw(int xx, int yy, Swiat swiat){
        super(xx, yy, 204, 2, 1, "Zolw", 1, swiat);
    }
    @Override
    public void akcja(){
        Random generator=new Random();
        int pom=generator.nextInt(4);
        if (pom == 0) {
            super.akcja();
        }
        else {
            this.setWiek(this.getWiek()+1);
            int zmeczony=this.getZmeczony();
            if (zmeczony > 0)
                this.setZmeczony(zmeczony--);
        }
    }
    @Override
    public int kolizja(Zwierze kolizujacy){
        JTextArea textArea=pochodzenie.getTextArea();
		if (kolizujacy.getId() == this.getId()){
			if (kolizujacy.getZmeczony() == 0 && this.getZmeczony() == 0){
				this.setZmeczony(6);
				return Komunikaty.ROZMNAZAJ.getVal();
			}
			else return Komunikaty.NIC.getVal();
		}
		else if (kolizujacy.getSila()-5 >= this.getSila()){
			return Komunikaty.ZABIJ.getVal();
		}
		else if (kolizujacy.getSila() < this.getSila()){
			return Komunikaty.UMIERAJ.getVal();
		}
		else {
                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" broni sie przed atakiem agresora\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
                    return Komunikaty.NIC.getVal();  
                }
	}
}

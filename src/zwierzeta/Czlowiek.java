/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zwierzeta;

import javax.swing.JTextArea;
import zwierzeta.Zwierze;
import organizm.Organizm;
import projektpo.Swiat;
import projektpo.Swiat.Komunikaty;

/**
 *
 * @author MychauU
 */
public class Czlowiek extends Zwierze{
    int moc;
    boolean niesmiertelonsc;
    boolean szybkosc;
    boolean nietykalnosc;
    int powerbuff;
    int cooldown;
    public Czlowiek(int xx, int yy, Swiat swiat){
                super(xx, yy, 155056 % 5, 5, 4, "Czlowiek", 1, swiat);
		moc=0;
		nietykalnosc=false;
		niesmiertelonsc=false;
		szybkosc=false;
		powerbuff=0;
		cooldown=0;
    }
    @Override
    public char rysowanie(){
		return 'H';
    }
    @Override
    public void akcja(){
		int max_x = pochodzenie.getXsize();
		int max_y = pochodzenie.getYsize();
		if (powerbuff!=0) {
			powerbuff--;
			setSila(getSila()-1);
		}
		if (cooldown!=0) {
                    cooldown--;
                }
		Organizm []organizmy = pochodzenie.getOrg();
		Organizm []kolejnosc = pochodzenie.getKol();
		int size_active = pochodzenie.getSizeOfHeap();
                JTextArea textArea=pochodzenie.getTextArea();
		int zdarzenie;
                int xpom=getX();
                int ypom=getY();
                String ruch_akcja=getRuch();
		if (ruch_akcja == "Up"){
			if (ypom > 0){
				if ((organizmy[xpom + (ypom - 1)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x);
					setY(ypom-1);
				}
				else {
					zdarzenie = organizmy[xpom + (ypom - 1)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x);
						setY(ypom-1);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom - 1)*max_x].getId()>=100 && organizmy[xpom + (ypom - 1)*max_x].getId()<200) || organizmy[xpom + (ypom - 1)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom - 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 1)*max_x].getX()+" i "+organizmy[xpom + (ypom - 1)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom - 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 1)*max_x].getX()+" i "+organizmy[xpom + (ypom - 1)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom + (ypom - 1)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom - 1)*max_x);
						setY(ypom-1);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
                                            if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                            textArea.setCaretPosition(textArea.getDocument().getLength());  
                                            organizmy[xpom + (ypom)*max_x] = null;
                                            return;
					}
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom - 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom - 1)*max_x].getId()>=100 && organizmy[xpom + (ypom - 1)*max_x].getId()<200) || organizmy[xpom + (ypom - 1)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom - 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 1)*max_x].getX()+" i "+organizmy[xpom + (ypom - 1)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom - 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom - 1)*max_x].getX()+" i "+organizmy[xpom + (ypom - 1)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom + (ypom - 1)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());
					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal());
				}
			}
		}
		else if (ruch_akcja == "Down"){
			if (ypom < max_y - 1){
				if ((organizmy[xpom + (ypom + 1)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x);
					setY(ypom+1);
				}
				else {
					zdarzenie = organizmy[xpom + (ypom + 1)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x);
						setY(ypom+1);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom + 1)*max_x].getId()>=100 && organizmy[xpom + (ypom + 1)*max_x].getId()<200) || organizmy[xpom + (ypom + 1)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom + 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 1)*max_x].getX()+" i "+organizmy[xpom + (ypom + 1)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom + 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 1)*max_x].getX()+" i "+organizmy[xpom + (ypom + 1)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom + (ypom + 1)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + (ypom + 1)*max_x);
						setY(ypom+1);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
						organizmy[xpom + (ypom)*max_x] = null;
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						return;
					}
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + (ypom + 1)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom + (ypom + 1)*max_x].getId()>=100 && organizmy[xpom + (ypom + 1)*max_x].getId()<200) || organizmy[xpom + (ypom + 1)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom + (ypom + 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 1)*max_x].getX()+" i "+organizmy[xpom + (ypom + 1)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom + (ypom + 1)*max_x].getNazwa() + " na pozycji "+organizmy[xpom + (ypom + 1)*max_x].getX()+" i "+organizmy[xpom + (ypom + 1)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom + (ypom + 1)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());
					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal());
				}
			}
		}
		else if (ruch_akcja == "Left"){
			if (xpom > 0){
				if ((organizmy[xpom - 1 + (ypom)*max_x] == null)){
					Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x);
					setX(xpom-1);
				}
				else {
					zdarzenie = organizmy[xpom - 1 + (ypom)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x);
						setX(xpom-1);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom - 1 + (ypom)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
						if ((organizmy[xpom -1+ (ypom)*max_x].getId()>=100 && organizmy[xpom -1+ (ypom)*max_x].getId()<200) || organizmy[xpom -1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom -1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -1+ (ypom)*max_x].getX()+" i "+organizmy[xpom -1+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom -1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -1+ (ypom)*max_x].getX()+" i "+organizmy[xpom -1+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom - 1 + (ypom)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom - 1 + (ypom)*max_x);
						setX(xpom-1);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
						organizmy[xpom + (ypom)*max_x] = null;
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						return;
					}
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom - 1 + (ypom)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom -1+ (ypom)*max_x].getId()>=100 && organizmy[xpom -1+ (ypom)*max_x].getId()<200) || organizmy[xpom -1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom -1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -1+ (ypom)*max_x].getX()+" i "+organizmy[xpom -1+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom -1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom -1+ (ypom)*max_x].getX()+" i "+organizmy[xpom -1+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());  
						organizmy[xpom - 1 + (ypom)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());
					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal());
				}
			}
		}
		else if (ruch_akcja == "Right"){
			if (xpom < max_x - 1){
				if ((organizmy[xpom + 1 + (ypom)*max_x] == null)){
					pochodzenie.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x);
					setX(xpom+1);
				}
				else {
					zdarzenie = organizmy[xpom + 1 + (ypom)*max_x].kolizja(this);
					if (zdarzenie == Komunikaty.UCIEKL.getVal()){
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x);
						setX(xpom+1);
					}
					else if (zdarzenie == Komunikaty.ZABIJ.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + 1 + (ypom)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom +1+ (ypom)*max_x].getId()>=100 && organizmy[xpom +1+ (ypom)*max_x].getId()<200) || organizmy[xpom +1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom +1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +1+ (ypom)*max_x].getX()+" i "+organizmy[xpom +1+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom +1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +1+ (ypom)*max_x].getX()+" i "+organizmy[xpom +1+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom + 1 + (ypom)*max_x] = null;
						Swiat.swappointers(organizmy,xpom + (ypom)*max_x,xpom + 1 + (ypom)*max_x);
						setX(xpom+1);
					}
					else if (zdarzenie == Komunikaty.UMIERAJ.getVal()){
						organizmy[xpom + (ypom)*max_x] = null;
                                                if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						return;
					}
					else if (zdarzenie == Komunikaty.ZUM.getVal()){
						for (int i = 0; i < size_active; i++){
							if (organizmy[xpom + 1 + (ypom)*max_x] == kolejnosc[i]){
								kolejnosc[i] = null;
								break;
							}
						}
                                                if ((organizmy[xpom +1+ (ypom)*max_x].getId()>=100 && organizmy[xpom +1+ (ypom)*max_x].getId()<200) || organizmy[xpom +1+ (ypom)*max_x].getId()<10){
                                                    textArea.append(organizmy[xpom +1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +1+ (ypom)*max_x].getX()+" i "+organizmy[xpom +1+ (ypom)*max_x].getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(organizmy[xpom +1+ (ypom)*max_x].getNazwa() + " na pozycji "+organizmy[xpom +1+ (ypom)*max_x].getX()+" i "+organizmy[xpom +1+ (ypom)*max_x].getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						if ((this.getId()>=100 && this.getId()<200) || this.getId()<10){
                                                    textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" ginie\n");
                                                }
                                                else {
                                                     textArea.append(this.getNazwa() + " na pozycji "+this.getX()+" i "+this.getY() +" zostaje zjedzony\n");
                                                }
                                                textArea.setCaretPosition(textArea.getDocument().getLength());
						organizmy[xpom + 1 + (ypom)*max_x] = null;
						organizmy[xpom + (ypom)*max_x] = null;
						return;
					}
					else if (zdarzenie == Komunikaty.NIC.getVal());

					else if (zdarzenie == Komunikaty.ROZMNAZAJ.getVal());
				}
			}
		}
		else if (ruch_akcja == "Power"){
			castSkill();
		}
	}
    @Override
	public int kolizja(Zwierze kolizujacy){
            JTextArea textArea=pochodzenie.getTextArea();
		if (kolizujacy.getSila() >= getSila()){
			return Komunikaty.ZABIJ.getVal();
		}
		else if (kolizujacy.getSila() < getSila()){
			return Komunikaty.UMIERAJ.getVal();
		}
                else return Komunikaty.NIC.getVal();
	}
	private void castSkill(){
            JTextArea textArea=pochodzenie.getTextArea();
		if (cooldown == 0){
			if (getId() == 1){
				risePower();
			}
		}
		else {
                    textArea.append("Umiejętność magiczny eliksir na cooldownie!\n");
                    textArea.setCaretPosition(textArea.getDocument().getLength());
		}
	}
	private void risePower(){
            JTextArea textArea=pochodzenie.getTextArea();
		cooldown = 10;
		setSila(getSila()+ 5);
		powerbuff = 5;
		textArea.append("Umiejętność 'magiczny eliksir' włączona!\n");
                textArea.setCaretPosition(textArea.getDocument().getLength());

	}
}
